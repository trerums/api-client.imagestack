<?php

namespace Orderbynull;

use GuzzleHttp\Client;

class ImageStack
{
    private $config;
    private $client;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->client = new Client();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getUploadUrl()
    {
        if(!isset($this->config['serviceHost']))
        {
            throw new \Exception('serviceHost is not set');
        }

        return $this->config['serviceHost'] . 'upload';
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getClientId()
    {
        if(!isset($this->config['clientId']))
        {
            throw new \Exception('clientid is not set');
        }

        return $this->config['clientId'];
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getSecret()
    {
        if(!isset($this->config['secret']))
        {
            throw new \Exception('secret is not set');
        }

        return $this->config['secret'];
    }

    /**
     * @param $filePath
     * @return mixed
     * @throws \Exception
     */
    public function upload($filePath)
    {
        $response = $this->client->post($this->getUploadUrl(), [
            'multipart' => [
                [
                    'name'     => 'clientId',
                    'contents' => $this->getClientId()
                ],
                [
                    'name'     => 'secret',
                    'contents' => $this->getSecret()
                ],
                [
                    'name'     => 'image',
                    'contents' => fopen($filePath, 'r')
                ]
            ]
        ]);

        return json_decode($response->getBody()->getContents());
    }
}